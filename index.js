const fp = require('fastify-plugin')
const {join, dirname} = require('path')
const {promisify} = require('util')
const {readFile} = require('fs')
const mkdirp = promisify(require('mkdirp'))
const {keygen} = require('tls-keygen')

const read = promisify(readFile)

async function tlsKeygen (fastify, {
  key = join(process.cwd(), 'key.pem'),
  cert = join(process.cwd(), 'cert.pem')
} = {}) {
  if (typeof fastify.server.addContext !== 'function') {
    throw new Error(
      'TLS is not enabled. Use the Fastify option: `https: true`'
    )
  }
  if (!fastify.server.key && !fastify.server.cert) {
    try {
      [key, cert] = await Promise.all([
        read(key),
        read(cert)
      ])
    } catch (error) {
      if (error.code === 'ENOENT') {
        const paths = await keygen({key, cert})
        await Promise.all([
          mkdirp(dirname(paths.cert)),
          mkdirp(dirname(paths.key))
        ]);
        [key, cert] = await Promise.all([
          read(paths.key),
          read(paths.cert)
        ])
      } else {
        throw error
      }
    }
    fastify.server.addContext('*', {key, cert})
  }
}

module.exports = fp(tlsKeygen)
