const fastify = require('fastify')
const plugin = require('.')
const server = fastify({
  logger: true,
  http2: true,
  https: true
})
server.register(plugin, {
  key: './private-key.pem',
  cert: './public-cert.pem'
})
server.listen(8443)
// Open a browser to see if it works?
// curl --verbose --insecure https://localhost:8443/ ; echo
