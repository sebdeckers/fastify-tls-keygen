# fastify-tls-keygen

Fastify plugin to automatically generate a browser-compatible, trusted, self-signed, localhost-only, TLS certificate.

|         | Chrome | Safari | Edge | Firefox |
|---------|:------:|:------:|:----:|:-------:|
| MacOS   |   ✅   |    ✅   |      |         |
| Windows |   ✅   |         |  ✅  |         |
| Linux   |   ✅   |         |      |    ✅   |

## Installation

```
npm install fastify-tls-keygen
```

## Usage

```js
const fastify = require('fastify')({
  // Required: Enable TLS
  https: true,
  // Optional: Enable HTTP/2
  http2: true
})

fastify.register(require('fastify-tls-keygen'), {
  // Optional (default: ./key.pem)
  key: '/path/to/save/private/key.pem',
  // Optional (default: ./cert.pem)
  cert: '/path/to/save/public/certificate.pem'
})

// Tip: Port 443 (HTTPS) requires root permissions. Use a port >1024.
fastify.listen(8443)
```

## See Also

- [tls-keygen](https://www.npmjs.com/package/tls-keygen) @ NPM
- [self-cert](https://github.com/jsumners/self-cert) @ NPM
- [LetsEncrypt](https://letsencrypt.org)

## Colophon

Made with 💝 by [Sebastiaan Deckers](https://twitter.com/sebdeckers) in 🇸🇬 Singapore.
